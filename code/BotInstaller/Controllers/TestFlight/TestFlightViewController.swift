//
//  TestFlightViewController.swift
//  BotInstaller
//
//  Created by Xiu on 16/5/6.
//  Copyright © 2016年 Neva. All rights reserved.
//

import UIKit
import Alamofire

class TestFlightViewController: UITableViewController, NSXMLParserDelegate {
    
    var element = NSMutableString()
    
    var dataItems = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        
        refresh()

    }
    
    func setup() {
        
        refreshControl?.addTarget(self, action: #selector(BotsViewController.refresh), forControlEvents: UIControlEvents.ValueChanged)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BotsViewController.refresh), name: UIApplicationWillEnterForegroundNotification, object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    /**
     *  刷新数据 
     */
    func refresh() {
        
        if let refreshControl = refreshControl where refreshControl.refreshing == false {
            refreshControl.beginRefreshing()
        }
        
        request(.GET, "http://git.ym/iOS/YMApp-Release/src/master/iOS/Product", parameters: nil)
            .responseJSON { response in
                if let xmlStr = String(data: response.data!, encoding: NSUTF8StringEncoding) {
                    // 获取tbody标签内的内容
                    let tbody = xmlStr.componentsSeparatedByString("<tbody>").last?.componentsSeparatedByString("</tbody>").first
                    let  a = "<xiu>" + tbody! + "</xiu>"
                    //开始解析
                    let data = a.dataUsingEncoding(NSUTF8StringEncoding)
                    let parserXML = NSXMLParser(data: data!)
                    parserXML.delegate = self
                    parserXML.parse()
                }
                self.refreshControl?.endRefreshing()
                self.tableView.reloadData()
        }
    }
    
    // 监听解析节点的属性
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]){
        if elementName == "a" && attributeDict["href"]?.componentsSeparatedByString(".ipa").count > 1{
            if let info = attributeDict["href"] {
                dataItems.append(info)
            }
        }
    }

    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataItems.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        
        cell.textLabel?.text = dataItems[indexPath.row].componentsSeparatedByString("/").last

        return cell
    }
 
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("push", sender: dataItems[indexPath.row])
    }


    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? TestFlightInstallViewController {
            vc.info = sender as? String
        }
    }
}
