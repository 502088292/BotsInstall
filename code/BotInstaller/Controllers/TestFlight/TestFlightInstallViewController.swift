//
//  TestFlightInstallViewController.swift
//  BotInstaller
//
//  Created by Xiu on 16/5/6.
//  Copyright © 2016年 Neva. All rights reserved.
//

import UIKit
import Alamofire
import zipzap
import Swifter
import SVProgressHUD

class TestFlightInstallViewController: UITableViewController {

    var info: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }

    @IBAction func sender(sender: UIBarButtonItem) {
        
//        let destination = Request.suggestedDownloadDestination(directory: .DocumentDirectory, domain: .UserDomainMask)
        if let str = info?.stringByReplacingOccurrencesOfString("/src/", withString: "/raw/") {
            let url = NSURL(string:"http://git.ym" + str)!
            SSAppInstaller.sharedInstance.installAPP(url, progress: { (progress) in
                SVProgressHUD.showProgress(progress)
                print(progress);
                SVProgressHUD.showProgress(progress, status: String(format: "下载中%.1f%%", progress*100))
                if progress == 1.0 {
                    SVProgressHUD.dismiss()
                } else if progress == -1 {
                    SVProgressHUD.showErrorWithStatus("❌下载失败")
                }
            })
        }
//            download(.GET, url, destination: destination)
//                .progress { bytesRead, totalBytesRead, totalBytesExpectedToRead in
//                    dispatch_async(dispatch_get_main_queue()) {
//                        print("Total bytes read on main queue: \(Float(totalBytesRead)/Float(totalBytesExpectedToRead))")
//                    }
//                }
//                .response { _, _, _, error in
//                    if let error = error {
//                        print("Failed with error: \(error)")
//                    } else {
////                        var a = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).last?.stringByAppendingString("/iAuto360_4.5.0_build_215.api")
////                        UIApplication.sharedApplication().openURL(NSURL(string:a!)!)
//                    }
//            }
//        }
//        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
