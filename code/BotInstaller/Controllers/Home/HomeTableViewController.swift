//
//  HomeTableViewController.swift
//  BotInstaller
//
//  Created by Xiu on 16/5/5.
//  Copyright © 2016年 Neva. All rights reserved.
//

import UIKit
import Alamofire

class HomeTableViewController: UITableViewController,NSXMLParserDelegate {
    let  dataItems = ["测试线","灰度线"]

    override func viewDidLoad() {
        super.viewDidLoad()
    }


    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataItems.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        cell.textLabel?.text = dataItems[indexPath.row];
        return cell
    }
 
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 1 {
            self.navigationController?.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("TestFlightViewController"), animated: true)
        }else if indexPath.row == 0 {
            self.navigationController?.pushViewController(UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("BotsViewController"), animated: true)
        }
    }
}
